using Godot;
using System.Collections.Generic;
using System.Linq;

public class Enemy : Humanoid
{
	public enum UnitType
	{
		Swordsman,
		Unarmed
	}
	UnitType unitType = UnitType.Swordsman;
	Area2D AggroArea;
	Timer AttackTimer;
	Humanoid attacking = null;
	List<Vector2> waypoints = new List<Vector2>();
	public Treasure MovingToTreasure = null;
	Timer TreasureGrabTimer;

	public static Enemy GetSwordsmanInstance()
	{
		return (Enemy)ResourceLoader.Load<PackedScene>("res://Scenes/Humanoids/Enemy.tscn").Instance();
	}
	public static Enemy GetUnarmedInstance()
	{
		return (Enemy)ResourceLoader.Load<PackedScene>("res://Scenes/Humanoids/UnarmedEnemy.tscn").Instance();
	}
	public override void _Ready()
	{

		Team = 1;
		base._Ready();
		if (!Sword.Visible)
		{
			unitType = UnitType.Unarmed;
		}
		AttackTimer = GetNode<Timer>("AttackTimer");
		AggroArea = GetNode<Area2D>("Area_Aggro");
		TreasureGrabTimer = GetNode<Timer>("TreasureGrabTimer");
	}

	public override void _Process(float delta)
	{
		if (Health <= 0 && null != MovingToTreasure && MovingToTreasure.CarriedBy == this)
		{
			MovingToTreasure.Reset();
			ForgetTreasure();
		}
		if (null == attacking)
		{
			direction = Vector2.Down;
			walkDir = Vector2.Zero;
			if (null != MovingToTreasure && ((null != MovingToTreasure.CarriedBy && MovingToTreasure.CarriedBy != this)
			|| (null != MovingToTreasure.BeingTakenBy && MovingToTreasure.BeingTakenBy != this)))
			{
				ForgetTreasure();
			}			
						
			if ((waypoints.Count <= 0 && TreasureGrabTimer.TimeLeft <= 0) || null == MovingToTreasure)
			{
				Node treasureParent = GetNode("/root/Game/Treasure");
				if (null == MovingToTreasure && treasureParent.GetChildCount() > 0)
				{
					Treasure closestTreasure = null;
					float closestDistSquared = float.MaxValue;
					
					for (int i = 0; i < treasureParent.GetChildCount(); i++)
					{
						if (null == treasureParent.GetChild<Treasure>(i).CarriedBy && null == treasureParent.GetChild<Treasure>(i).BeingTakenBy)
						{
							float distSquared = Position.DistanceSquaredTo(treasureParent.GetChild<Treasure>(i).Position);
							if (distSquared < closestDistSquared)
							{
								closestTreasure = treasureParent.GetChild<Treasure>(i);
								closestDistSquared = distSquared;
							}
						}
					}
					MovingToTreasure = closestTreasure;
					if (null != MovingToTreasure)
					{
						NavigateTo(MovingToTreasure.Position);
					}
					else
					{
						NavigateTo(GetNode<Player>("/root/Game/Player").Position);
					}
				}
				else
				{
					NavigateToExit();
				}
			}


			if (waypoints.Count > 0)
			{
				if (Position.DistanceSquaredTo(waypoints[0]) < 64)
				{
					waypoints.RemoveAt(0);
				}
			}
			if (waypoints.Count > 0)
			{
				walkDir = Position.DirectionTo(waypoints[0]);
			}

			if (MovingToTreasure != null && MovingToTreasure.CarriedBy == null && Position.DistanceSquaredTo(MovingToTreasure.Position) < 64)
			{
				walkDir = Vector2.Zero;
				if (TreasureGrabTimer.TimeLeft == 0 && null == MovingToTreasure.BeingTakenBy)
				{
					TreasureGrabTimer.Start();
					MovingToTreasure.BeingTakenBy = this;
				}
			}
		}
		else
		{
			if (null == MovingToTreasure || null == MovingToTreasure.CarriedBy || MovingToTreasure.CarriedBy != this)
			{
				ForgetTreasure();
			}
			direction = Position.DirectionTo(attacking.Position);
			walkDir = direction;
		}


		base._Process(delta);

	}

	private void ForgetTreasure()
	{
		waypoints.Clear();
		if (null != MovingToTreasure && null != MovingToTreasure.BeingTakenBy && MovingToTreasure.BeingTakenBy == this)
		{
			MovingToTreasure.BeingTakenBy = null;
		}
		MovingToTreasure = null;

		TreasureGrabTimer.Stop();
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		attacking = null;


		if (unitType == UnitType.Unarmed)
		{
			attacking = null;
		}
		else
		{
			if (null != attacking && !AggroArea.OverlapsBody(attacking))
			{
				attacking = null;
			}
			if (null == attacking)
			{
				Godot.Collections.Array bodies = AggroArea.GetOverlappingBodies();

				for (int i = 0; i < bodies.Count; i++)
				{
					if (bodies[i] is Humanoid && ((Humanoid)bodies[i]).Team != Team)
					{
						attacking = ((Humanoid)bodies[i]);
						if (AttackTimer.TimeLeft <= 0)
						{
							AttackTimer.Start(prng.RandfRange(0.3f, 2.0f));
						}
						break;
					}
				}
			}
		}
	}

	private void NavigateTo(Vector2 targetPosition)
	{
		waypoints = GetNode<Navigation2D>("/root/Game/World/Navigation2D").GetSimplePath(Position, targetPosition).ToList();
		waypoints.RemoveAt(0);
	}

	private void NavigateToExit()
	{
		Vector2 closestExit = Vector2.Zero;
		if (Position.x >= 990)
		{
			closestExit = new Vector2(1920f, 595f);
		}
		else
		{
			closestExit = new Vector2(0f, 595f);
		}
		if (Position.DistanceSquaredTo(closestExit) > Position.DistanceSquaredTo(new Vector2(950, 1080)))
		{
			closestExit = new Vector2(950, 1080);
		}
		NavigateTo(closestExit);
	}

	private void _on_AttackTimer_timeout()
	{
		if (null != attacking)
		{
			SlashSword();
		}
	}

	private void _on_TreasureGrabTimer_timeout()
	{
		if (null != MovingToTreasure)
		{
			MovingToTreasure.CarriedBy = this;
		}
		NavigateToExit();
	}
}






