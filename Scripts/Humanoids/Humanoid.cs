using Godot;
using System;

public class Humanoid : KinematicBody2D
{
	public int Team { get; protected set; } = 0;

	protected float accuracyFactor = 0.1f;
	protected Vector2 direction = Vector2.Down;
	Node2D Head;
	protected Sprite Sword;
	protected Timer SwordTimer;
	Vector2 originalSwordPosition;
	protected Sprite Shield;
	float originalShieldWidth;
	Vector2 originalShieldPosition;
	Vector2 originalShieldOffset;

	protected Sprite Bow;
	Sprite Arrow;
	Timer BowTimer;
	float originalBowWidth;
	Vector2 originalBowPosition;

	AnimationPlayer AnimationPlayer;
	protected float walkSpeed = 50;

	Vector2 Impulse;

	protected bool Blocking = false;

	protected Vector2 walkDir = Vector2.Zero;
	public int Health = 3;

	protected RandomNumberGenerator prng = new RandomNumberGenerator();
	Node slashAudioParent;
	Node blockAudioParent;
	Node impactAudioParent;

	private static Node GetGhostInstance()
	{
		return ResourceLoader.Load<PackedScene>("res://Scenes/Humanoids/Ghost.tscn").Instance();
	}
	public void SetRandomSeed(uint seed)
	{
		prng.Seed = (ulong)seed;
	}
	public override void _Ready()
	{	
		Head = GetNode<Node2D>("YSort/Head");

		if (HasNode("YSort/Sword"))
		{
			Sword = GetNode<Sprite>("YSort/Sword");
			originalSwordPosition = Sword.Position;
			SwordTimer = Sword.GetNode<Timer>("SwordTimer");

			Shield = GetNode<Sprite>("YSort/Shield");
			originalShieldWidth = Shield.Scale.x;
			originalShieldPosition = Shield.Position;
			originalShieldOffset = Shield.Offset;

			Bow = GetNode<Sprite>("YSort/Bow");
			originalBowPosition = Bow.Position;
			originalBowWidth = Bow.Scale.x;
			BowTimer = Bow.GetNode<Timer>("BowTimer");
			Arrow = Bow.GetNode<Sprite>("Arrow");

			slashAudioParent = GetNode("Audio_Slash");
			blockAudioParent = GetNode("Audio_Block");
			impactAudioParent = GetNode("Audio_Impact");
		}


		AnimationPlayer = GetNode<AnimationPlayer>("AnimationPlayer_Body");
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		if (Health <= 0)
		{
			direction = Vector2.Down;
			walkDir = Vector2.Zero;
			Blocking = false;
		}
		Head.GetNode<Sprite>("Rear").Visible = direction.y < 0;
		float rot = direction.Angle();

		Head.Position = new Vector2(direction.x, 3);

		if (null != Sword)
		{
			float shieldWidth = Mathf.Abs(Mathf.Sin(rot));
			shieldWidth = Mathf.Clamp(shieldWidth, 0.2f, 1);

			Shield.Scale = new Vector2(shieldWidth, 1) * originalShieldWidth;
			Shield.Position = (originalShieldPosition + (Blocking ? new Vector2(-2, 0) : Vector2.Zero)).Rotated(rot - Mathf.Pi * 0.5f);
			Shield.Offset = originalShieldOffset + (Blocking ? new Vector2(0, -4) : Vector2.Zero);

			Sword.Position = originalSwordPosition.Rotated(rot - Mathf.Pi * 0.5f);

			float bowWidth = Mathf.Cos(rot);
			bowWidth = Mathf.Clamp(Mathf.Abs(bowWidth), 0.2f, 1) * Mathf.Sign(bowWidth);
			Bow.Position = originalBowPosition.Rotated(rot - Mathf.Pi * 0.5f);
			Bow.Scale = new Vector2(1, bowWidth) * originalBowWidth;

		}
	}

	public override void _PhysicsProcess(float delta)
	{
		if (Health > 0)
		{
			if (Impulse.LengthSquared() < 10)
			{
				if (walkDir.LengthSquared() > 0)
				{
					walkDir = walkDir.Normalized();

					if (AnimationPlayer.CurrentAnimation != "Walk")
					{
						AnimationPlayer.Play("Walk");

					}
				}
				else if (AnimationPlayer.CurrentAnimation != "Idle")
				{
					AnimationPlayer.Play("Idle");
				}
			}
			if (Impulse.LengthSquared() < 10 || walkDir.LengthSquared() > 0)
			{
				Vector2 velocity = (walkDir * walkSpeed + Impulse);

				KinematicCollision2D collision = MoveAndCollide(velocity * delta);
				if (null != collision)
				{
					if (collision.Collider is KinematicBody2D)
					{

						((KinematicBody2D)collision.Collider).MoveAndSlide(-collision.Normal * velocity.Length());

					}
					else if (collision.Collider is StaticBody2D)
					{
						velocity = velocity.Slide(collision.Normal);
						MoveAndSlide(velocity);
					}
				}
				//MoveAndSlide(walkDir * walkSpeed + Impulse);
				Impulse -= Impulse * 5 * delta;
			}

		}

	}

	protected void SlashSword(int swordLevel = 1)
	{
		if (null != SwordTimer && SwordTimer.TimeLeft <= 0 && Health > 0 && !Blocking)
		{
			float rot = direction.Angle();
			Slash slash = Slash.GetInstance(Team, swordLevel);
			AddChild(slash);
			slash.Rotation = rot;
			Sword.Rotation = rot + Mathf.Pi * 0.25f;
			SwordTimer.Start();
			int soundIndex = prng.RandiRange(0,slashAudioParent.GetChildCount() - 1);
			slashAudioParent.GetChild<AudioStreamPlayer>(soundIndex).Play();
		}
	}

	protected void FireArrows(Vector2 targetPosition, int bowLevel = 1)
	{
		if (null != BowTimer && BowTimer.TimeLeft <= 0 && Health > 0)
		{
			int soundIndex = prng.RandiRange(0,slashAudioParent.GetChildCount() - 1);
			slashAudioParent.GetChild<AudioStreamPlayer>(soundIndex).Play();
			Arrow.Visible = false;
			float startRadius = 20;
			float endRadius = Position.DistanceTo(targetPosition) * accuracyFactor;
			for (int i = 0; i < bowLevel; i++)
			{
				Vector2 endPosition = targetPosition + Vector2.Right.Rotated(prng.RandfRange(-Mathf.Pi, Mathf.Pi)) * prng.RandfRange(0, endRadius);
				Vector2 startPosition = Position + Vector2.Right.Rotated(prng.RandfRange(-Mathf.Pi, Mathf.Pi)) * prng.RandfRange(0, startRadius);
				FlyingArrow arrow = FlyingArrow.GetInstance(startPosition, endPosition, Team);
				GetNode("/root/Game/Arrows").AddChild(arrow);
			}

			BowTimer.Start();
		}
	}

	public void GetHit(int damage, Vector2 impulse)
	{
		if (Blocking && direction.Dot(impulse.Normalized()) < -0.7f)
		{
			Impulse += impulse * 0.1f;
			CPUParticles2D hitParticles = Slash.GetSlashHitInstance();
			hitParticles.GlobalPosition = Position - impulse.Normalized() * 5;
			GetNode("/root/Game/FxContainer").AddChild(hitParticles);
			hitParticles.Emitting = true;
			int soundIndex = prng.RandiRange(0,blockAudioParent.GetChildCount() - 1);
			blockAudioParent.GetChild<AudioStreamPlayer>(soundIndex).Play();
		}
		else
		{
			int soundIndex = prng.RandiRange(0,impactAudioParent.GetChildCount() - 1);
			impactAudioParent.GetChild<AudioStreamPlayer>(soundIndex).Play();
			Impulse += impulse;
			Health -= damage;
			if(this is Player)
			{
				GetNode<Game>("/root/Game").SetHealth(Health);
			}
			if (Health <= 0)
			{
				if(this is Enemy)
				{
					GetNode<Game>("/root/Game").RemoveEnemy();
				}
				else if(this is Player)
				{
					GetNode<Game>("/root/Game").ShowDefeatScreen();
				}
				AnimationPlayer.Play("Die");
				GetNode<Timer>("DeathTimer").Start();
				GetNode<CollisionShape2D>("CollisionShape2D").SetDeferred("disabled", true);
				Node ghost = GetGhostInstance();
				AddChild(ghost);
			}
			else
			{
				AnimationPlayer.Play("GetHit");
			}

		}
	}

	private void _on_SwordTimer_timeout()
	{
		Sword.Rotation = Mathf.Pi * -0.25f;
	}

	private void _on_BowTimer_timeout()
	{
		Arrow.Visible = true;
	}
	
	private void _on_DeathTimer_timeout()
	{
		QueueFree();
	}
}






