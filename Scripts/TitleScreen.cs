using Godot;
using System;

public class TitleScreen : Control
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Input.SetMouseMode(Input.MouseMode.Visible);
	}

	private void _on_Button_Start_pressed()
	{
		GetTree().ChangeScene("res://Scenes/Intro.tscn");
	}
	
	private void _on_Button_Exit_pressed()
	{
		GetTree().Quit();
	}
}






