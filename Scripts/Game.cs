using Godot;
using System;

public class Game : YSort
{
	private int[][] waves = new int[][]//first index is wave id. per wave, the first int is the number of unarmed enemies, the second is the number of swordsmen
	{
		new int[]{3,1},
		new int[]{4,2},
		new int[]{6,4},
		new int[]{6,8},
		new int[]{0,16},
		new int[]{0,20},
		new int[]{10,20},
		new int[]{10,40},
		new int[]{20,40},
		new int[]{30,50},
	};
	int enemiesLeft = 0;
	int treasureLeft = 3;
	int currentWave = -1;

	RandomNumberGenerator prng;
	float radius = 50;
	Vector2[] spawnPositions = new Vector2[] {
		new Vector2(100,600),
		new Vector2(1820,600),
		new Vector2(880,980),
	};
	Node enemyParent;

	Player player;

	Control skillPanel;
	Control victoryScreen;
	Control defeatScreen;
	public override void _Ready()
	{
		prng = new RandomNumberGenerator();
		Input.SetMouseMode(Input.MouseMode.Hidden);
		enemyParent = GetNode("Enemies");
		skillPanel = GetNode<Control>("CanvasLayer/Control/SkillPanel");
		victoryScreen = GetNode<Control>("CanvasLayer/Control/VictoryScreen");
		victoryScreen.Visible = false;
		defeatScreen = GetNode<Control>("CanvasLayer/Control/DefeatScreen");
		defeatScreen.Visible = false;

		player = GetNode<Player>("Player");
		
		StartNextWave();
		SetTreasureCount();
	}

	public void RemoveEnemy()
	{
		int tempEnemies = 0;
		for (int i = 0; i < enemyParent.GetChildCount(); i++)
		{
			if (enemyParent.GetChild<Enemy>(i).Health > 0)
			{
				tempEnemies++;
			}
		}
		enemiesLeft = tempEnemies;
		SetEnemyCount();
	}

	public void RemoveTreasure()
	{
		treasureLeft -= 1;
		if(treasureLeft <= 0)
		{
			ShowDefeatScreen();
		}
		SetTreasureCount();
	}

	public override void _Process(float delta)
	{
		if (enemiesLeft <= 0 && !skillPanel.Visible)
		{
			if (waves.Length > currentWave + 1)
			{
				skillPanel.Visible = true;
				Input.SetMouseMode(Input.MouseMode.Visible);
			}
			else if (!victoryScreen.Visible && !defeatScreen.Visible)
			{
				ShowVictoryScreen();
			}

		}
	}


	private void StartNextWave()
	{		
		skillPanel.Visible = false;
		Input.SetMouseMode(Input.MouseMode.Hidden);
		currentWave += 1;
		SetWaveCount();
		if (waves.Length > currentWave)
		{
			for (int i = 0; i < waves[currentWave][0]; i++)
			{
				Enemy enemy = Enemy.GetUnarmedInstance();
				enemy.Position = spawnPositions[prng.RandiRange(0, spawnPositions.Length - 1)] + Vector2.Right.Rotated(prng.RandfRange(-Mathf.Pi, Mathf.Pi) * prng.RandfRange(0, radius));
				enemiesLeft += 1;
				enemy.SetRandomSeed(prng.Randi());
				enemyParent.AddChild(enemy);
			}
			for (int i = 0; i < waves[currentWave][1]; i++)
			{
				Enemy enemy = Enemy.GetSwordsmanInstance();
				enemy.Position = spawnPositions[prng.RandiRange(0, spawnPositions.Length - 1)] + Vector2.Right.Rotated(prng.RandfRange(-Mathf.Pi, Mathf.Pi) * prng.RandfRange(0, radius));
				enemiesLeft += 1;
				enemy.SetRandomSeed(prng.Randi());
				enemyParent.AddChild(enemy);
			}
		}
		SetEnemyCount();
	}

	private void SetEnemyCount()
	{
		GetNode<Label>("CanvasLayer/Control/EnemiesLeft").Text = enemiesLeft.ToString();
	}

	private void SetTreasureCount()
	{
		GetNode<Label>("CanvasLayer/Control/TreasureLeft").Text = treasureLeft.ToString();
	}

	private void SetWaveCount()
	{
		GetNode<Label>("CanvasLayer/Control/HBoxContainer/WaveCount").Text = (currentWave + 1).ToString();
	}

	public void SetHealth(int health)
	{
		GetNode<Label>("CanvasLayer/Control/Health").Text = health.ToString();
	}

	private void ShowVictoryScreen()
	{
		GetNode<AudioStreamPlayer>("Music").SetVolumeDb(-18);
		victoryScreen.Visible = true;
		Input.SetMouseMode(Input.MouseMode.Visible);
		victoryScreen.GetNode<AnimationPlayer>("AnimationPlayer").Play("Victory");
	}

	public void ShowDefeatScreen()
	{
		GetNode<AudioStreamPlayer>("Music").SetVolumeDb(-18);
		defeatScreen.Visible = true;
		Input.SetMouseMode(Input.MouseMode.Visible);
		defeatScreen.GetNode<AnimationPlayer>("AnimationPlayer").Play("Defeat");
	}

	private void _on_Button_Speed_pressed()
	{
		player.SpeedLevel += 1;
		skillPanel.GetNode<Label>("MarginContainer/VBoxContainer/CenterContainer/GridContainer/Label_SpeedLevel").Text = player.SpeedLevel.ToString();
		StartNextWave();
	}


	private void _on_Button_Sword_pressed()
	{
		player.SwordLevel += 1;
		skillPanel.GetNode<Label>("MarginContainer/VBoxContainer/CenterContainer/GridContainer/Label_SwordLevel").Text = player.SwordLevel.ToString();
		StartNextWave();
	}



	private void _on_Button_Bow_pressed()
	{
		player.BowLevel += 1;
		skillPanel.GetNode<Label>("MarginContainer/VBoxContainer/CenterContainer/GridContainer/Label_BowLevel").Text = player.BowLevel.ToString();
		StartNextWave();
	}

	private void _on_Button_Restart_pressed()
	{
		GetTree().ReloadCurrentScene();
	}


	private void _on_Button_Exit_pressed()
	{
		GetTree().ChangeScene("res://Scenes/Screens/TitleScreen.tscn");
	}
	
	private void _on_InfoScreenTimer_timeout()
	{
		GetNode<Control>("CanvasLayer/Control/InfoScreen").Visible = false;
	}
}








