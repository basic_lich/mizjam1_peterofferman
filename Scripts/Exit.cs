using Godot;
using System;

public class Exit : YSort
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{

	}


	private void _on_Area_Exit_body_entered(object body)
	{
		if(body is Enemy)
		{
			Enemy enemy = (Enemy)body;
			if(null != enemy.MovingToTreasure && null != enemy.MovingToTreasure.CarriedBy && enemy.MovingToTreasure.CarriedBy == enemy)
			{
				GetNode<Game>("/root/Game").RemoveEnemy();
				GetNode<Game>("/root/Game").RemoveTreasure();
				enemy.MovingToTreasure.QueueFree();
				enemy.QueueFree();
			}
		}
	}
}


