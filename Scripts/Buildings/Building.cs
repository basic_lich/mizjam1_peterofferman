using Godot;
using System;

public class Building : StaticBody2D
{
	private static Color TransparentColor = new Color(1.0f, 1.0f, 1.0f, 0.5f);
	private Area2D area;
	public override void _Ready()
	{
		area = GetNode<Area2D>("Area2D");
	}

	public void _on_Area2D_body_entered(object body)
	{
		if (body is Humanoid)
		{
			Modulate = TransparentColor;
		}
	}


	public void _on_Area2D_body_exited(object body)
	{
		if (body is Humanoid)
		{
			bool humanoidFound = false;
			Godot.Collections.Array bodies = area.GetOverlappingBodies();
			for (int i = 0; i < bodies.Count; i++)
			{
				if (bodies[i] is Humanoid && bodies[i] != body)
				{
					humanoidFound = true;
					break;
				}
			}
			if(!humanoidFound)
			{
				Modulate = Colors.White;
			}
		}
	}

}
