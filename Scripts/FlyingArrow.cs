using Godot;
using System;

public class FlyingArrow : Node2D
{
	int Team;
	Path2D Path;
	Path2D ShadowPath;

	PathFollow2D Arrow;
	PathFollow2D Shadow;
	Vector2 startPosition;
	Vector2 endPosition;

	Area2D hitArea;
	bool finished = false;

	float flightSpeed = 500;

	public static FlyingArrow GetInstance(Vector2 start, Vector2 end, int team)
	{
		FlyingArrow arrow = (FlyingArrow)ResourceLoader.Load<PackedScene>("res://Scenes/FlyingArrow.tscn").Instance();
		arrow.Team = team;
		arrow.startPosition = start;
		arrow.endPosition = end;
		arrow.CalculateTrajectory();

		return arrow;
	}

	public void SetupScene()
	{
		Path = GetNode<Path2D>("Path2D");
		Path.Curve = (Curve2D)Path.Curve.Duplicate();
		ShadowPath = GetNode<Path2D>("Path2D_Shadow");
		ShadowPath.Curve = (Curve2D)ShadowPath.Curve.Duplicate();

		Arrow = Path.GetNode<PathFollow2D>("PathFollow2D");
		hitArea = Arrow.GetNode<Area2D>("Area2D");
		Shadow = ShadowPath.GetNode<PathFollow2D>("PathFollow2D");		
	}

	public override void _Ready()
	{
		if (null == Path)
		{
			SetupScene();
		}
	}

	public override void _Process(float delta)
	{
		Arrow.Offset += flightSpeed * delta;
		Shadow.UnitOffset = Arrow.UnitOffset;
	}

	public override void _PhysicsProcess(float delta)
	{
		if(!finished && Arrow.GlobalPosition.DistanceSquaredTo(endPosition) < 5)
		{
			finished = true;
			Godot.Collections.Array bodies = hitArea.GetOverlappingBodies();
			for(int i = 0; i < bodies.Count; i++)
			{
				if(bodies[i] is Humanoid && ((Humanoid)bodies[i]).Team != Team)
				{
					((Humanoid)bodies[i]).GetHit(1, startPosition.DirectionTo(endPosition) * 100);
					QueueFree();
					break;
				}
			}
			Shadow.Visible = false;
			GetNode<AudioStreamPlayer>("AudioStreamPlayer").Play();
		}
		
	}

	private void CalculateTrajectory()
	{
		if (null == Path)
		{
			SetupScene();
		}
		float distance = startPosition.DistanceTo(endPosition);
		Vector2 halfOffset = (endPosition - startPosition) * 0.5f;
		Path.Curve.ClearPoints();
		Path.Curve.AddPoint(startPosition);
		Path.Curve.AddPoint(startPosition + halfOffset + new Vector2(0, -distance * 0.2f), halfOffset * -0.5f, halfOffset * 0.5f);
		Path.Curve.AddPoint(endPosition);
		Arrow.Offset = 0;

		ShadowPath.Curve.ClearPoints();
		ShadowPath.Curve.AddPoint(startPosition);
		ShadowPath.Curve.AddPoint(endPosition);
		Shadow.Offset = 0;
	}
	private void _on_AudioStreamPlayer_finished()
{
	QueueFree();
}
}



