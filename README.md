**RAIDERS OF MIZBURGH**
You are a knight of Mizburgh. All your knight buddies have left to plunder other towns, and they've left you to defend the city. This game was completed within 24 hours of work, spread across 6 days. It's an entry for MizJam1: a gamejam where each game uses the same art assets.
The game is available to play on [itch.io](https://peterofferman.itch.io/raiders-of-mizburgh)

**GOAL**
Protect the treasures of Mizburgh from the looters. Enemies will try to take the amazing treasures, like the diamond, the crown, and the cyclops' eye. I don't know why they kept the eye, but whatever.
Stay alive, and keep at least 1 treasure in the city. Endure 10 waves of enemies, upgrade your skills between waves, and emerge victorious.

**CONTROLS**

| Button | Action |
| ------ | ------ |
| WASD/Arrow keys | Move around |
| Aim | Mouse |
| Select sword/shield | 1 |
| Select bow | 2 |
| Attack | Left mouse button |
| Block (shield) | Right mouse button |

**ABOUT ME**

My name is Peter Offerman, and I make video games for fun. Because I'm unable to support myself and my family with this amazing hobby, I make a living by programming business software.
[peterofferman.com](http://www.peterofferman.com)
[Twitter.com/GamesByPeter](https://www.twitter.com/gamesbypeter)